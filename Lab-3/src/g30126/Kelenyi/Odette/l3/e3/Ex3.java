package g30126.Kelenyi.Odette.l3.e3;

import becker.robots.*;

public class Ex3
{
   public static void main(String[] args)
   {  
   	// Set up the initial situation
   	City prague = new City();
   	Robot karel = new Robot(prague, 5, 5, Direction.NORTH);
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
   }
} 