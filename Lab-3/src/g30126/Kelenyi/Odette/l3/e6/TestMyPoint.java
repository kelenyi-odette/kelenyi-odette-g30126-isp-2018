package g30126.Kelenyi.Odette.l3.e6;


public class TestMyPoint {
    public static void main(String[] args) {

        MyPoint p1 = new MyPoint(); //constructs and initializes a point at the origin (0, 0)
        MyPoint p2 = new MyPoint(2,5);  //constructs and initializes a point at (2, 5)

        //displays the points coordinates
        System.out.println(p1);
        System.out.println(p2);
        System.out.println("----");

        //sets the coordinates for p1 at (1, 4)
        p1.setX(1);
        p1.setY(4);
        System.out.println(p1);
        System.out.println("----");

        //sets the coordinates for p1 at (3, 8) using the method for both x and y
        p1.setXY(3,8);
        System.out.println(p1);
        System.out.println("----");

        //prints a string description of the instance in the format �(x, y)�
        System.out.println(p1.toString());
        System.out.println("----");

        System.out.println("The distance from this point to another point at the given (x, y) coordinates:");
        System.out.println(p1.distance(2,5));

        System.out.println("The distance between this point and a given MyPoint instance:");
        System.out.println(p1.OverloadDistance(p2));
    }
}
