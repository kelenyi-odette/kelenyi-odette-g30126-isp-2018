package g30126.Kelenyi.Odette.l3.e5;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class Ex5
{
private static Wall blockAve0;
private static Thing parcel;
private static Wall blockAve1;
private static Wall blockAve3;
private static Wall blockAve5;
private static Wall blockAve6;
private static Wall blockAve4;
private static Wall blockAve2;

public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      setParcel(new Thing(ny, 2, 2));
      setBlockAve0(new Wall(ny, 1, 1, Direction.WEST));
      setBlockAve1(new Wall(ny, 2, 1, Direction.WEST));
      setBlockAve2(new Wall(ny, 1, 1, Direction.NORTH));
      setBlockAve3(new Wall(ny, 1, 2, Direction.NORTH));
      setBlockAve4(new Wall(ny, 2, 1, Direction.SOUTH));
      setBlockAve5(new Wall(ny, 1, 2, Direction.SOUTH));
      setBlockAve6(new Wall(ny, 1, 2, Direction.EAST));

      Robot karel = new Robot(ny, 1, 2, Direction.SOUTH);
 
 
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.move();
      karel.pickThing();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      karel.turnLeft();
   }

public static Wall getBlockAve0() {
	return blockAve0;
}

public static void setBlockAve0(Wall blockAve0) {
	Ex5.blockAve0 = blockAve0;
}

public static Thing getParcel() {
	return parcel;
}

public static void setParcel(Thing parcel) {
	Ex5.parcel = parcel;
}

public static Wall getBlockAve1() {
	return blockAve1;
}

public static void setBlockAve1(Wall blockAve1) {
	Ex5.blockAve1 = blockAve1;
}

public static Wall getBlockAve3() {
	return blockAve3;
}

public static void setBlockAve3(Wall blockAve3) {
	Ex5.blockAve3 = blockAve3;
}

public static Wall getBlockAve5() {
	return blockAve5;
}

public static void setBlockAve5(Wall blockAve5) {
	Ex5.blockAve5 = blockAve5;
}

public static Wall getBlockAve6() {
	return blockAve6;
}

public static void setBlockAve6(Wall blockAve6) {
	Ex5.blockAve6 = blockAve6;
}

public static Wall getBlockAve4() {
	return blockAve4;
}

public static void setBlockAve4(Wall blockAve4) {
	Ex5.blockAve4 = blockAve4;
}

public static Wall getBlockAve2() {
	return blockAve2;
}

public static void setBlockAve2(Wall blockAve2) {
	Ex5.blockAve2 = blockAve2;
}
}
