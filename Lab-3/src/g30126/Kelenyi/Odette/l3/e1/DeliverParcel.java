package g30126.Kelenyi.Odette.l3.e1;

import becker.robots.*;

public class DeliverParcel
{
   public static void main(String[] args)
   {  
   	// Set up the initial situation
   	City prague = new City();
      Robot karel = new Robot(prague, 1, 0, Direction.EAST);
 
		// Direct the robot to the final situation
      karel.move();
      karel.move();
      karel.pickThing();
      karel.move();
      karel.turnLeft();	// start turning right as three turn lefts
      karel.turnLeft();
      karel.turnLeft();	// finished turning right
      karel.move();
      karel.putThing();
      karel.move();
   }
}

