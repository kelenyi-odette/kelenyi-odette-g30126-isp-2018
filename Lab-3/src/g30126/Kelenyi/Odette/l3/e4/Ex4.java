package g30126.Kelenyi.Odette.l3.e4;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class Ex4
{
private static Wall blockAve0;
private static Wall blockAve1;
private static Wall blockAve2;
private static Wall blockAve3;
private static Wall blockAve4;
private static Wall blockAve5;
private static Wall blockAve6;
private static Wall blockAve7;

public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      setBlockAve0(new Wall(ny, 3, 2, Direction.WEST));
      setBlockAve1(new Wall(ny, 4, 2, Direction.WEST));
      setBlockAve2(new Wall(ny, 3, 2, Direction.NORTH));
      setBlockAve3(new Wall(ny, 3, 3, Direction.NORTH));
      setBlockAve4(new Wall(ny, 4, 2, Direction.SOUTH));
      setBlockAve5(new Wall(ny, 4, 3, Direction.SOUTH));
      setBlockAve6(new Wall(ny, 3, 3, Direction.EAST));
      setBlockAve7(new Wall(ny, 4, 3, Direction.EAST));
      Robot mark = new Robot(ny, 2, 4, Direction.WEST);
 
 
      // mark goes around the roadblock
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
     
   }

public static Wall getBlockAve0() {
	return blockAve0;
}

public static void setBlockAve0(Wall blockAve0) {
	Ex4.blockAve0 = blockAve0;
}

public static Wall getBlockAve1() {
	return blockAve1;
}

public static void setBlockAve1(Wall blockAve1) {
	Ex4.blockAve1 = blockAve1;
}

public static Wall getBlockAve2() {
	return blockAve2;
}

public static void setBlockAve2(Wall blockAve2) {
	Ex4.blockAve2 = blockAve2;
}

public static Wall getBlockAve3() {
	return blockAve3;
}

public static void setBlockAve3(Wall blockAve3) {
	Ex4.blockAve3 = blockAve3;
}

public static Wall getBlockAve4() {
	return blockAve4;
}

public static void setBlockAve4(Wall blockAve4) {
	Ex4.blockAve4 = blockAve4;
}

public static Wall getBlockAve5() {
	return blockAve5;
}

public static void setBlockAve5(Wall blockAve5) {
	Ex4.blockAve5 = blockAve5;
}

public static Wall getBlockAve6() {
	return blockAve6;
}

public static void setBlockAve6(Wall blockAve6) {
	Ex4.blockAve6 = blockAve6;
}

public static Wall getBlockAve7() {
	return blockAve7;
}

public static void setBlockAve7(Wall blockAve7) {
	Ex4.blockAve7 = blockAve7;
}
}

