package g30126.Kelenyi.Odette.l3.ProblemaTest;

public class Solution {
    public static int solution(int[] A) {

        boolean solutia = true;

        for(int i = 0; i < A.length; i++){ 
        	solutia = true;
            for(int j = 0; j < A.length; j++) 
                if(A[i] == A[j] && i != j){ 
                	solutia = false; 
                    break; 
            }
            if(solutia) 
                return A[i]; 

        }
        return 0;
    }
    public static void main(String[] args) {
        int[] A = new int[7];
        A[0] = 9;  A[1] = 3;  A[2] = 9;
        A[3] = 3;  A[4] = 3;  A[5] = 7;
        A[6] = 9;
        int result = solution(A);

        if(result==7)
            System.out.println("The result it's corect.");
        else
            System.out.println("The result it's incorect.");
    }
}
