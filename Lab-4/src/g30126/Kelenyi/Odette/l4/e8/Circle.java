package g30126.Kelenyi.Odette.l4.e8;

class Circle extends Shape{
	private double radius;
	
	public Circle(){
		radius = 1;
	}
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public Circle(double radius, String color, boolean filled) {
		super();
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getArea() {
		return Math.PI*getRadius()*getRadius();
	}
	
	public double getPerimeter() {
		return 2*Math.PI*getRadius();
	}
	
	public String toString() {
		return "A Circle with radius=" + getRadius() + ", which is a subclass of " + super.toString();
	}
	
}
