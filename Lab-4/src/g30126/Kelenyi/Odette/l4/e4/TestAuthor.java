package g30126.Kelenyi.Odette.l4.e4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestAuthor {

	@Test
	void testToString() {
		System.out.println("Execute test toString");
		Author a = new Author("John", "John@email.com",'m');
		assertEquals(a.toString(),"author-John(m) at John@email.com");
	}

}
