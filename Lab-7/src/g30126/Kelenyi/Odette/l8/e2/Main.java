package g30126.Kelenyi.Odette.l8.e2;

public class Main {
	
	public static void main(String[] args) {
		Bank bank = new Bank();
		bank.addAccount("Sam", 109.98);
		bank.addAccount("Ema", 111.97);
		bank.addAccount("Dia", 209.88);
		bank.addAccount("Debi", 123.55);
		
		bank.printAccounts();
		bank.printAccounts(51,120);
		bank.getAccount("Semi");
		bank.getAllAccounts();
	}

}
