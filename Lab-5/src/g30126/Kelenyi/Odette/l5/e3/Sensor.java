package g30126.Kelenyi.Odette.l5.e3;

public abstract class Sensor {
	private String location;
	
	public abstract int readValue();
	
	public String getLocation() {
		return location;
	}

}
