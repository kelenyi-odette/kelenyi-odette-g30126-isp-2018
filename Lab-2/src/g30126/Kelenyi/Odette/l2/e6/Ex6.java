package g30126.Kelenyi.Odette.l2.e6;

import java.util.Scanner;

public class Ex6 {
    public static void fact_nerec(int n){
        int fact=1;
        for(int i=1;i<=n;i++)
            fact=fact*i;
        System.out.println("n!="+fact);
    }
    public static int fact_rec(int n){
        int rez;
        if ((n==1)||(n==0)) return 1;
        rez = fact_rec(n-1)*n;
        return rez;
    }
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.print("n=");
        int n=in.nextInt();
        System.out.print("Non recursive method: ");
        fact_nerec(n);
        System.out.print("Recursive method: ");
        System.out.println("n!=" +fact_rec(n));
        in.close();
    }
}
