package g30126.Kelenyi.Odette.l2.e4;

import java.util.Random;


public class Ex4 {
	public static void main(String[] args){
		
		int[] num = {1,2,3,4,5};
		int i;
		Random r = new Random();
    	for(i=0;i<5;i++){
    		num[i]=r.nextInt(30);
    	}
		
		int maxe=-1000;
		for(i=0;i<num.length;i++)
		{
			if(num[i]>maxe)
				maxe=num[i];
		}
		System.out.println(maxe);
	}
}
