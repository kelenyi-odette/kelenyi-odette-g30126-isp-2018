package g30126.Kelenyi.Odette.l2.e5;

import java.util.Random;

public class Ex5 {
	static void bubbleSort(int[] array) {  
        int n = array.length;  
        int temp = 0;  
         for(int i=0; i < n; i++){  
                 for(int j=1; j < (n-i); j++){  
                          if(array[j-1] > array[j]){   
                                 temp = array[j-1];  
                                 array[j-1] = array[j];  
                                 array[j] = temp;  
                         }  
                          
                 }  
         }  
  
    }  
    public static void main(String[] args) {  
                int array[] ={3,60,35,2,45,320,5,123,8,11,121};  
                int i;
                Random r = new Random();
            	for(i=0;i<9;i++){
            		array[i]=r.nextInt(30);
            	}
                 
                System.out.println("Array Before Bubble Sort");  
                for(i=0; i < array.length; i++){  
                System.out.print(array[i] + " ");  
                }  
                System.out.println();  
                  
                bubbleSort(array); //sorting array elements using bubble sort  
                 
                System.out.println("Array After Bubble Sort");  
                for(i=0; i < array.length; i++){  
                        System.out.print(array[i] + " ");  
                }  
   
        }  
}

