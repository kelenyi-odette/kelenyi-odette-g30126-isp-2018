package g30126.Kelenyi.Odette.l6.e5;

import java.awt.Color;
import java.awt.Graphics;

import g30126.Kelenyi.Odette.l6.e1.DrawingBoard;
import g30126.Kelenyi.Odette.l6.e1.Shape;

public class Pyramid extends Rectangle{
	private int bricks;
	private int length;
	private int width;
	

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public Pyramid(Color color, int length, int width, int bricks){
		super(color, length, width);
		this.bricks = bricks; 
		this.length = length;
		this.width = width;
	}
	
	@Override
	public void draw(Graphics g) {
		System.out.println("Drawing a pyramid with " + bricks + " bricks.");
		int nr = 0;
		int p;
		int i = 0;
		int linie, coloana;
		while(i+nr<bricks) {
			i++;
			nr = nr + i; 
			p=i;
			while(p !=  0) {
				
				for(linie = 0; linie<=i;linie++) {
					for(coloana=0; coloana <linie; coloana++) {
						System.out.println("Drawing!");
						g.drawRect(((DrawingBoard.WIDTH)/2 - (linie*(getLength()/2) - (coloana*getLength()))),(40 + linie*getWidth()) , getLength(), getWidth());
						System.out.println(getLength()+" " + getWidth() + nr);
					}
				}
				
				p--;
			}
		}
	}
	
	
	public static void main(String[] args) {
		DrawingBoard b = new DrawingBoard();
		
		Shape p = new Pyramid(Color.BLUE, 100, 50, 56);
		b.addShape(p);
		System.out.println();
		
	}
}