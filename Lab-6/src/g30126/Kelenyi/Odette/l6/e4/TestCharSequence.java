package g30126.Kelenyi.Odette.l6.e4;

import org.junit.Test;

class TestCharSequenceClass {
	
	char[] car = {'v', 'w', 'x', 'y', 'z'};
	CharSequenceClass cr = new CharSequenceClass(car);
	
	@Test
	void testCharAt() {
		assertEquals('x',cr.charAt(2));
	}

	@Test
	void testLength() {
		assertEquals(5,cr.length());
	}

	private void assertEquals(int i, int length) {
		// TODO Auto-generated method stub
		
	}

	private void assertEquals(String string, CharSequence charSequence) {
		// TODO Auto-generated method stub
		
	}

	@Test
	void testSubSequence() {
		assertEquals("xyz",cr.subSequence(2, 5));
	}

}

