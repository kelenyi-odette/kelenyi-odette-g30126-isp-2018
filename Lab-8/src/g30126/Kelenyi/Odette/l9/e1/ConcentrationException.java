package g30126.Kelenyi.Odette.l9.e1;

class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c,String msg) {
          super(msg);
          this.c = c;
    }

    int getConc(){
          return c;
    }
}

