package g30126.Kelenyi.Odette.l9.e4;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
       Car c1 = new Car("Peugeot",3000);
       Car c2 = new Car("Nissan",13000);
       Car c3 = new Car("Tesla",100000);
       c1.saveCar("carPeugeot");
       c2.saveCar("carNissan");
       c3.saveCar("carTesla");

      try {
           Car co = Car.load("carPeugeot");
           System.out.println(co);
          Car ca = Car.load("carNissan");
          System.out.println(ca);
          Car ct = Car.load("carTesla");
          System.out.println(ct);
       } catch (IOException e) {
            e.printStackTrace();
       } catch (ClassNotFoundException e) {
           e.printStackTrace();
        }

    }
}